//Andy Chen & Dennis Gelman
//CSC225 - Lab 8 - Mammen

#include "myGrep.h"

//typedef struct wordInfo Word;
struct Word{
	
	char* line;
	int lineNum;
	int wordPos;
	struct Word* next;
	
};

int main(int argc, char *argv[]) 
{
	
	status(argc,argv);
	read_file(argv);
	
	return 0;
	
}

void status(int argc, char* argv[])
{
	
	if(argc != 3) 
	{
		
		printf("myGrep: improper number of arguments \n");
		printf("Usage: ./a.out <filename> <word> \n");
		exit(EXIT_FAILURE);
	}
	
	printf("%s %s %s\n", argv[0], argv[1], argv[2]);
}

void read_file(char* argv[1])
{
	FILE* file;
	file = fopen(argv[1],"r");
	char line[MAX_SIZE];
	char* lineCopy;
	int lineNum = 0;
	int longestCount = 0;
	char* longestLine;
	char* token;
	struct Word* head = NULL;
	struct Word* temp;
	int wordPos = 0;
	int wordCount = 0;
		
	if(file == NULL)
	{
		printf("Cannot open file: %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}
	
	while(fgets(line,MAX_SIZE,file) != NULL)
	{
		
		//Count the longest line and number of chars in it
		if(longestCount < strlen(line))
		{
			longestCount = strlen(line);
			//Allocate space for longestLine and copy line into it NOT including '\n'
			longestLine = (char*)malloc(sizeof(char)*longestCount);
			strncpy(longestLine,line,longestCount-1);
			longestLine[longestCount-1] = '\0';	//Add terminatingn '\0'
		}		
		
		//Allocate space for lineCopy and copy line into it NOT including '\n'
		lineCopy = (char*)malloc(sizeof(char)*strlen(line));
		strncpy(lineCopy,line,strlen(line)-1);
		lineCopy[strlen(line)-1] = '\0';	//Add terminating '\0'
		
		//Parse line with tokens
		token = strtok(line," .,!?\n");
		wordPos = 0;
		
		//Create new Word object for each occurence of word
		while(token != NULL) 
		{
			
			if(strcmp(token,argv[2]) == 0) 
			{
				//Allocate space for a Word struct
				temp = (struct Word*)malloc(sizeof(struct Word));
				(*temp).next = head;
				(*temp).line = (char*)malloc(sizeof(char)*(strlen(lineCopy)+1));	//+1 to create space for strcpy append '\0' at the end of line
				//Copy to line in structure
				strcpy((*temp).line,lineCopy);
				(*temp).lineNum = lineNum;
				(*temp).wordPos = wordPos;
				head = temp; 
				wordCount++;				
			}	
			
			token = strtok(NULL," .,!?\n");
			wordPos++;
			
		}
		
		lineNum++;

	}
	
	//Print out longest line info
	printf("longest line: %s\n",longestLine);
	printf("num chars: %d\n", longestCount);
	printf("num lines: %d\n",lineNum);
	printf("total occurrences of word: %d\n",wordCount);
		
	//Print out the linked list (backwards) using a stack
	struct Word* walker;
	struct Word* wordStack[wordCount];
	int top = -1;
	walker = head;
	while(walker != NULL) 
	{ 
		wordStack[++top] = walker;
		walker = walker->next;
	}
	while(top > -1)
	{
		printf("line %d; word %d; %s\n", wordStack[top]->lineNum,wordStack[top]->wordPos,wordStack[top]->line);
		top--;
	}
	
	//Close file
	fclose(file);
	
}
