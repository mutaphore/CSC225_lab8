//Andy Chen & Dennis Gelman
//CSC225 - Lab 8 - Mammen

#ifndef MYGREP_H
#define MYGREP_H
#define MAX_SIZE 100
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void read_file(char* argv[]);
void status(int argc, char* argv[]);
#endif
